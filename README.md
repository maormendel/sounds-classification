# Sounds Classification 

Using mel-spectrogram, CNN and linear classifier, I build an AI that classifies 10 different sounds from the Urban Sounds 8K dataset..
 - I also used in data augmentation techniques like time and frequency masking, shifting, etc, to increase the size of the datset and the diversty to improve perfomences.

Results: 73% accurcy in the test set(1746 samples)
